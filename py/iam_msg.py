#  ========================================================================
#  Copyright (c) 2013 The University of Washington
# 
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
# 
#      http://www.apache.org/licenses/LICENSE-2.0
# 
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#  ========================================================================
#

#
# IAM messaging tools (AWS edition)
#

# AWS interface classes 
from boto.sqs.connection import SQSConnection
from boto.sqs.message import RawMessage
from boto.sns import SNSConnection

# crypto class covers for openssl
import M2Crypto
from M2Crypto import BIO, RSA, EVP, X509

# connection pooling http client
import urllib3
from urllib3.connectionpool import HTTPSConnectionPool

# json classes
import simplejson as json
import uuid

import datetime
import dateutil.parser
import base64
import string
import time
import re
import os.path
from sys import exit
import signal

import threading

# syslog shortcuts
import syslog

log=syslog.syslog
log_debug=syslog.LOG_DEBUG
log_info=syslog.LOG_INFO
log_err=syslog.LOG_ERR
log_alert=syslog.LOG_ALERT

# ----- global vars (mostly from config file) ------------------

# config structure
iamConfig = None

# decryption keys
iamCryptKeys = {}

# public keys used for sig verify
iamPubKeys = {}

# private keys used for sig sign
iamPvtKeys = {}

# AWS connections
snsConnection = None 
sqsConnection = None 
sqsQueue = None

#
# -------------------------------------
#
def save_message_and_exit(message):
   f = open('failed_message.txt','a')
   f.write(message)
   f.close()
   exit(1) 

def getSnsConnection():
   global iamConfig 
   global snsConnection

   aws = iamConfig['aws']
   snsConnection = SNSConnection(aws['snsKeyId'], aws['snsKey'])
   if snsConnection==None:
      print 'connect failed'
      exit (1)


def getSqsQueue():
   global iamConfig 
   global sqsConnection
   global sqsQueue

   aws = iamConfig['aws']
   sqsConnection = SQSConnection(aws['sqsKeyId'], aws['sqsKey'])
   sqsQueue = sqsConnection.get_queue(aws['sqsQueue'])
   if sqsQueue==None:
      log(log_alert, "Could not connect to '%s'!" % (aws['sqsQueue']))
      print "Could not connect to '%s'!" % (aws['sqsQueue'])
      exit (1)
   sqsQueue.set_message_class(RawMessage)
   log(log_info, '%r messages in the queue' % (sqsQueue.count()))
   print '%r messages in the queue' % (sqsQueue.count())

#
# accumulate header fields for signature
#
def buildSigMsg(header, txt):
  
   sigmsg = header[u'contentType'] + '\n'
   if 'keyId' in header:
      sigmsg = sigmsg + header[u'iv'] + '\n' + header[u'keyId'] + '\n'
   sigmsg = sigmsg + header[u'messageContext'] + '\n' + header[u'messageId'] + '\n' + \
       header[u'messageType'] + '\n' + header[u'sender'] + '\n' + \
       header[u'signingCertUrl'] + '\n' + header[u'timestamp'] + '\n' + header[u'version'] + '\n' + \
       txt + '\n'
   # print "sigmsg: " + sigmsg
   return sigmsg


#
# send an iam message
#
#  msg is anything
#  context is string

def iam_sendMessage(msg, context, cryptid, signid):
   
   if snsConnection==None:
      getSnsConnection()

   iamHeader = {}
   iamHeader['contentType'] = 'json'
   iamHeader['version'] = 'UWIT-1'
   iamHeader['messageType'] = 'iam-test'
   u = uuid.uuid4()
   iamHeader['messageId'] = str(u)
   iamHeader['messageContext'] = base64.b64encode(context)
   iamHeader['sender'] = 'gwsdev-pydemo'

   iamHeader['timestamp'] = datetime.datetime.utcnow().isoformat()
   iamHeader['signingCertUrl'] = iamPvtKeys[signid]['url']

   if cryptid!=None:
      iamHeader['keyId'] = cryptid
      iv = os.urandom(16)
      iamHeader['iv'] = base64.b64encode(iv)
      cipher = M2Crypto.EVP.Cipher(alg='aes_128_cbc', key=iamCryptKeys[cryptid], iv=iv, op=1)
      txt = cipher.update(msg) + cipher.final()
      enctxt64 = base64.b64encode(txt)
      # print 'enctxt64 = ' + enctxt64
   else:
      print 'message will not be encrypted'
      enctxt64 = base64.b64encode(msg)
   
   # gen the signature
   sigmsg = buildSigMsg(iamHeader, enctxt64)
   # print '[' + sigmsg + ']'

   key = iamPvtKeys[signid]['key']
   key.sign_init()
   key.sign_update(sigmsg)
   sig = key.sign_final()
   sig64 = base64.b64encode(sig)
   # print 'sig = ' + sig64
   iamHeader['signature'] = sig64

   body = {}
   body['Message'] = enctxt64
  
   iamMessage = {}
   iamMessage['header'] = iamHeader
   iamMessage['body'] = enctxt64

   # print iamMessage
   m64 = base64.b64encode(json.dumps(iamMessage))

   arn = iamConfig['aws']['snsArn']
   snsConnection.publish(arn, m64, 'subject')

   
#
# process an Iam message
#

def processIamMessage(message):
   global iamCryptKeys 
   global iamPubKeys 

   # get the body of the SQS message
   sqsstr = message.get_body().encode('utf8','ignore')  # signature, et.al. needs utf8
   # print 'sqsstr:' + sqsstr
   sqsmsg = json.loads(sqsstr)

   # get the iam message
   msgstr = base64.b64decode(sqsmsg['Message']).encode('utf8','ignore')
   # print 'msgstr:' + msgstr
   iamMsg = json.loads(msgstr)

   iamHeader = iamMsg['header']
   # print iamHeader

   try:
     # check the version
     if iamHeader[u'version'] != 'UWIT-1':
        log(log_err, 'unknown version: ' + iamHeader[u'version'])
        return

     # the signing cert should be cached most of the time
     certurl = iamHeader[u'signingCertUrl']
     if not certurl in iamPubKeys:
        log(log_info, 'Fetching signing cert: ' + certurl)
        pool = urllib3.connection_from_url(certurl)
        certdoc = pool.request('GET', certurl)
        if certdoc.status != 200:
           log(log_err, 'sws cert get failed: ' + certdoc.status)
           save_message_and_exit (sqsstr)  # can't go on
        log(log_info, 'got it')
        pem = certdoc.data
        x509 = X509.load_cert_string(pem)
        key = x509.get_pubkey()
        iamPubKeys[certurl] = key
     # print "got sig cert"

     enctxt64 = iamMsg[u'body']
     # print "emsg: " + enctxt64

     # check the signature
     sigmsg = buildSigMsg(iamHeader, enctxt64)
     # print 'sigmsg = [%s]' % sigmsg
     sig = base64.b64decode(iamHeader[u'signature'])
     # print 'sig = [%s]' % iamHeader[u'signature']
     pubkey = iamPubKeys[certurl]
     # print 'have pubkey from ' + certurl

     pubkey.reset_context(md='sha1')
     pubkey.verify_init()
     pubkey.verify_update(sigmsg)
     if pubkey.verify_final(sig)!=1:
        log(log_err, '*** signature fails verification ***')
        print "verify fails"
        # save_message_and_exit(sqsstr)  # can't go on
     # print 'sig verify ok'

     # decrypt the message
     if 'keyId' in iamHeader:
        iv64 = iamHeader[u'iv']
        # print "iv: " + iv64
        iv = base64.b64decode(iv64)
        keyid = iamHeader[u'keyId']
        # print 'looking for crypt key ' + keyid
        if not keyid in iamCryptKeys:
           print keyid + ' not found'
        key = iamCryptKeys[keyid]
 
        enctxt =  base64.b64decode(enctxt64)
        cipher = M2Crypto.EVP.Cipher(alg='aes_128_cbc', key=key, iv=iv, op=0)
        txt = cipher.update(enctxt) + cipher.final()
     else:
        txt = base64.b64decode(enctxt64)
        print 'message was not encrypted'

     txt = filter(lambda x: x in string.printable, txt)
     # print '[' + txt + ']'
     iamMsg[u'body'] = txt
     # un-base64 the context
     try:
        iamHeader[u'messageContext'] = base64.b64decode(iamHeader[u'messageContext'])
     except TypeError:
        print 'context not base64'
   except KeyError:
      if 'AlarmName' in iamMsg:
         log(log_debug, 'alarm: ' + iamMsg['AlarmName'])
         return True

      print 'keyerror'
      log(log_err, 'Unknown message: ' )
      return None

   return iamMsg


# read one message
def iam_awsRead():
   global sqsQueue

   if sqsQueue==None:
      getSqsQueue()

   message = sqsQueue.read()
   if message==None: 
      return None
   ret = processIamMessage(message)

   if (ret!=None):
      sqsQueue.delete_message(message)

   return ret


def iam_awsInit(cfg):
   global iamConfig
   global iamCryptKeys
   global iamPubKeys

   iamConfig = cfg

   # load the signing keys
   certs = iamConfig['certs']
   for c in certs:
      id = c['id']
      crt = {}
      crt['url'] = c['url']
      crt['key'] = EVP.load_key(c['keyfile'])
      iamPvtKeys[id] = crt


   # load the cryption key
   keys = iamConfig['crypts']
   for k in keys:
      id = k['id']
      k64 = k['key']
      # print 'adding crypt key ' + id
      kbin = base64.b64decode(k64)
      iamCryptKeys[id] = kbin



